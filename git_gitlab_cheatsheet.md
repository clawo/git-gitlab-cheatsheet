# Git and Gitlab Cheatsheet

# Create new local project and push it the first time
- Create new directory, add files etc.
- initialize git in that directory: run `git init` after changing to the dir
- Commit changes locally
- Run `git push --set-upstream git@gitlab.com:clawo/PROJECT_NAME.git master`
(replace clawo mit own username, test_gitlab with name of repo)
- add origin: `git remote add origin ssh://git@gitlab.com/clawo/PROJECT_NAME.git` (???)
- Then, git pull/push origin master should work
